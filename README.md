# Simple tools to simplify infra setup

### basic auth pwd
This image is used to generate credential for basic auth (useul for instance to protect a service behind a reverse proxy)


**How to use:**
```
docker run registry.gitlab.com/adrienbd/infra-tools-public/htpasswd -bBn myUserName myClearTextPwd
```
(-B: Bcrypt better than MD5 and SHA1)

NB:
- For usage in docker compose (cli flag for traefik, not needed if value set from env), $ should be escaped as $$
- in a docker compose, multiple user:ph can be listed: comma separated.
```
docker run registry.gitlab.com/adrienbd/infra-tools-public/htpasswd -bBn myUserName myClearTextPwd | sed -e s/\\$/\\$\\$/g
```

## DEV 

build and push to registry

### TODO
- automate builds in CI
